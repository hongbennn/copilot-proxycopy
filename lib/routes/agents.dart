import 'package:copilot_proxy/context.dart';

Future<void> getAgents(Context context) async {
  context.ok();
  context.json({'agents':[]});
}